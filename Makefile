.token:
	echo "enter token"
	cat >> .token
TOGGL_USER = $(shell cat .token)


.email:
	echo "enter email"
	cat >> .email
TOGGL_EMAIL = $(shell cat .email)

.workspace:
	echo "enter workspace id"
	cat >> .workspace
TOGGL_WORKSPACE ?= $(shell cat .workspace)
TOGGL_CLIENTID ?= "no client"

.since:
	echo "start date (YYYY-MM-DD)"
	cat >> .since
TOGGL_SINCE ?= $(shell cat .since)

TOGGL_API_URL ?= https://www.toggl.com/api/v9
TOGGL_REPORTS_API_URL ?= https://www.toggl.com/reports/api/v2

workspace-ids:  .token
	curl  \
		-u ${TOGGL_USER}:api_token \
		-H "Content-Type: application/json" \
		${TOGGL_API_URL}/me/workspaces \
		| jq '.[] | {"id": .id, "name": .name}'

client-ids:  .token
	curl  \
		-u ${TOGGL_USER}:api_token \
		-H "Content-Type: application/json" \
		${TOGGL_API_URL}/me/clients \
		| jq '.[] | {"id": .id, "name": .name}'

project-ids:  .token .workspace
	curl  \
		-u ${TOGGL_USER}:api_token \
		-H "Content-Type: application/json" \
		${TOGGL_API_URL}/me/projects \
		| jq '.[] | select(.client_id == "${TOGGL_CLIENTID}" )'

week-details.json: .token .email .workspace .since
	curl  \
		-u ${TOGGL_USER}:api_token \
		-H "Content-Type: application/json" \
		-X GET \
		"${TOGGL_REPORTS_API_URL}/details?workspace_id=${TOGGL_WORKSPACE}&since=${TOGGL_SINCE}&user_agent=${TOGGL_EMAIL}" \
		> $@

week-report: week-details.json toggl_to_table.py
	python3.8 toggl_to_table.py < week-details.json

week-report.tsv: week-details.json toggl_to_table.py
	python3.8 toggl_to_table.py < week-details.json > $@

clean:
	git clean -f