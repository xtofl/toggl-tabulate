#!/usr/bin/env python3.8
import itertools
from datetime import datetime
from collections import namedtuple
import json
from sys import stdin
from typing import *

# {
#    "id": 1663639189,
#    "pid": 162925100,
#    "tid": null,
#    "uid": 490158,
#    "description": "Actia HVAC spec ",
#    "start": "2020-08-21T07:29:43+02:00",
#    "end": "2020-08-21T08:20:30+02:00",
#    "updated": "2020-08-21T08:20:30+02:00",
#    "dur": 3047000,
#    "user": "Kristoffel Pirard",
#    "use_stop": true,
#    "client": "Van Hool",
#    "project": "hmi hvac",
#    "project_color": "0",
#    "project_hex_color": "#0b83d9",
#    "task": null,
#    "billable": null,
#    "is_billable": false,
#    "cur": null,
#    "tags": []
#  }


def format_row(entry):
    def format_time(t): return datetime.strftime(t, "%H:%M")
    elements = \
        f"{entry.project}: {entry.description}",\
        "",\
        format_time(entry.start),\
        format_time(entry.end),\
        entry.end - entry.start
    return "\t".join(map(str, elements))


Entry = namedtuple("Entry", "project description start end")


def to_entry(detail_json):
    project, description, start, end = \
        map(detail_json.get, "project description start end".split())
    return Entry(project, description,
                 datetime.fromisoformat(start), datetime.fromisoformat(end))


def to_row(entry):
    return format_row(entry)


def to_table(details: List[object]):
    entries = sorted(map(to_entry, details), key=lambda e: e.start)

    def rows():
        for day, day_entries in itertools.groupby(entries, lambda e: e.start.date()):
            yield str()
            yield str(day)
            yield from map(to_row, day_entries)

    return "\n".join(rows())


def main():
    objects = json.loads(stdin.read())["data"][-1:1:-1]
    print(to_table(objects))


if __name__ == "__main__":
    main()
